#include<stdio.h>
#include<string.h>
char T[9999], P[100];
int Q=0, A[125],  TABLE[100][125], i, j, k, l, lnth, m, count;
int F(int sk, char tk);

int main() {
    int len_p, K, S=0;
    printf("Enter String: ");
    gets(T);
    printf("Enter Search String: ");
    gets(P);
    len_p=strlen(P);
    for(K=0;T[K] && S !=len_p;K++) {
        S= F(S, T[K]);
    }
    if(S==len_p) {
        printf("Pattern found at index %d \n", K-len_p);
    }
    else {
        printf("Pattern not found");
    }
    return 0;
}

int F(int sk, char tk) {
    if(Q==0) {
        lnth=strlen(P);
        for(i=0;T[i];i++)
            A[T[i]]=1;
        for(i=0;i<lnth;i++) {
            for(j=0;j<125;j++) {
                if(A[i]==0)
                    continue;
                if(P[i]==j) {
                    TABLE[i][j]=(Q+=1);
                    continue;
                }
                for(k=i-1;k>=0;k--) {
                    if(P[k]==j) {
                        for(l=k-1,m=i-1,count=1;l>=0;l--,m--) {
                            if(P[l]==P[m])
                                count++;
                            else
                                break;
                        }
                        if(count==k+1) {
                            TABLE[i][j]=count;
                            break;
                        }
                    }
                }
            }
        }
        Q=1;
    }
    return TABLE[sk][tk];
}
