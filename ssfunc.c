#include<stdio.h>
int seqSearch(int A[], int n, int z, int index)
{
     int i=0;
    A[n]=z, index=-1;

    while(A[i]!=z)
    {
        i++;
    }
    if(i<n)
       index=i;
    return index;
}

int main()
{
    int arr[50], arrSize, item, i, result;
    printf("Please enter the array size: ");
    scanf("%d", &arrSize);
    printf("Please Enter the array elements: ");
    for(i=0;i<arrSize;i++)
    {
        scanf("%d", &arr[i]);
    }

    printf("Please enter the item you want to search: ");
    scanf("%d", &item);

    result= seqSearch(arr, arrSize, item, 2);

    if(result<0)
         printf("Sorry Item not found");
    else
        printf("Item found at %d position", result+1);
}
