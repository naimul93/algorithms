#include<stdio.h>
int main()
{
    int n,e,f,x;
    int i=0;
    int A[10];
    printf("Enter the number of elements you want to store in the array: ");
    scanf("%d", &n);
    printf("Please inter the elements: ");
    for(e=0;e<n;e++)
    {
        scanf("%d", &A[e]);
    }
    printf("So the elements are: ");
    for(f=0;f<n;f++)
    {
        printf(" %d", A[f]);
    }
    printf("\n");
    printf(" \nEnter the item you want to search in your array: ");
    scanf("%d", &x);

    while(i<n && (A[i]!=x))
    {
        i++;
    }
    printf("\n");
    if(i>=n)
        printf("Item not found");
    else
        printf("Item found at index %d", i);

    return 0;
}
