#include<bits/stdc++.h>
using   namespace   std;

class   edge
{
    int v,w;
public:
    edge(int    a,  int b)
    {
        v=a;
        w=b;                                                    //NOT   COMPLETED
    }
    int get_v()
    {
        return  v;
    }
    int get_w()
    {
        return  w;
    }
};

int main()
{
    freopen("inptw.txt",  "r",    stdin);
    freopen("outptw.txt",  "w",    stdout);
    int tn, n1, n2, w;
    cin>>tn;
    vector<list<edge>> adjlist(tn);
    while(cin>>n1>>n2>>w)
    {
        adjlist[n1].push_back(edge(n2,w));
    }
    vector<list<edge>>::iterator i;
    int x=0;
    cout<<"Adjacency List: \n";
    for(i=adjlist.begin();  i != adjlist.end(); i++)
    {
        cout<<x<<"--> ";
        list<edge> li=*i;
        list<edge>::iterator it;
        for(it=li.begin();it !=li.end();it++)
        {
            cout<<"["<<(*it).get_v<<", "<<(*it).get_w<<"]"<<" ";
        }
        cout<<endl;
        x++;
    }
}
