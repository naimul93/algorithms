#include<bits/stdc++.h>
using   namespace   std;
int main()
{
    freopen("inpt.txt",  "r",    stdin);
    freopen("outpt.txt",  "w",    stdout);
    int tn, n1, n2;
    cin>>tn;
    vector<list<int>>   adjlist(tn);
    while(cin>>n1>>n2)
    {
        adjlist[n1].push_back(n2);
    }
    vector<list<int>>::iterator i;
    int x=0;
    cout<<"Adjacency List: \n";
    for(i=adjlist.begin();  i != adjlist.end(); i++)
    {
        cout<<x<<"--> ";
        list<int> li=*i;
        list<int>::iterator it;
        for(it=li.begin();it !=li.end();it++)
        {
            cout<<*it<<"  ";
        }
        cout<<endl;
        x++;
    }
}
