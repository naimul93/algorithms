#include<stdio.h>
int main()
{
    int lb, ub, mid, itm, i;
    int A[13]= {11, 22, 30, 33, 40, 44, 55, 60, 66, 77, 80, 88, 99};
    itm=44;
    lb=0, ub=12;
    mid= (lb+ub)/2;
    while(lb<=ub && A[mid]!=itm)
    {
        if(itm<A[mid])
        {
            ub=mid-1;
        }
        else
        {
            lb=mid+1;
        }
        mid=(lb+ub)/2;
    }
    if(A[mid]==itm)
    {
        i=mid;
    }
    else
    {
        i=0;
    }

    if(i==0)
        printf("Item not found");
    else
        printf("Item found at index %d", i);

    return 0;
}
