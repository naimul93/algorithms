#include<stdio.h>
#include<string.h>
int main() {
    char P[]= "IMU";
    char T[]= "NAIMUL";
    int R= strlen(P);
    int S= strlen(T);
    int K=1;
    int MAX= S-R+1;
    int INDEX;
    while(K<=MAX) {
        int L=1;
        while(L<R) {
            if(P[L]!=T[K+L-1])
                break;
            L++;
        }
        if(L==R) {
            INDEX=K;
            break;
        }
        INDEX=0;
        K++;
    }
    if(INDEX==0)
        printf("Pattern didn't match");
    else
        printf("Pattern matched at index %d", INDEX);

    return 0;
}
